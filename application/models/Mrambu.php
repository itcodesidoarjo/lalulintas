<?php
	class Mrambu extends CI_Model
	{
		public function input_rambu($data)
		{
		   $this->db->insert('tbl_rambu', $data);
		   $id = $this->db->insert_id();
		 }

		public function dt_rambu()
		{
		    $query=$this->db->query("
		    	SELECT * FROM tbl_rambu where status_rambu = 1");
		   	return $query->result();
		}

		public function hapus($data,$where){
		  	$this->db->where($where);
			$this->db->update('tbl_rambu',$data);
		}


		public function dt_rambu_detail($id)
		{
		    $query=$this->db->query("
		    	SELECT * FROM tbl_rambu where status_rambu = 1 and id_rambu =".$id);
		   	return $query->result();
		} 

		public function update_rambu($data,$where){
		  	$this->db->where($where);
			$this->db->update('tbl_rambu',$data);
		} 

		public function dt_pdf_rambu($id)
		{
		    $query=$this->db->query("
		    	SELECT * FROM tbl_rambu where status_rambu = 1 and id_rambu =".$id);
		   	return $query->result();
		}
	}