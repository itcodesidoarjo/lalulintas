<?php
	class Mregisteradmin extends CI_Model
	{
		public function register($enc_password){
			// User data array
			date_default_timezone_set('Asia/Jakarta');

			$dat_user = array(
				'username'		=> $this->input->post('username'),
                'email_admin' 	=> $this->input->post('email_admin'),
                'password' 		=> $enc_password,
                'tglreg_admin'	=> date("Y-m-d h:i:sa"),
			);
			/*-------Mengambil id users dan mengirimkan ke model-----*/
			$id_akun = $this->tambah_akun($dat_user);
			/*-----------Menangkap Data Dari Form------------------*/
          	/*sukses*/
		  	$this->session->set_flashdata('pesan', 'Registrasi berhasil. Silahkan Login disini!.');
		  	redirect('admin'); //mengembalikan halaman setelah berhasil menginputkan data
		}
		
		public function upedit_ver($data,$where){
		  	$this->db->where($where);
			$this->db->update('tbl_admin',$data);
		} 

		public function tambah_akun($data)
	   	{
	       $this->db->insert('tbl_admin', $data);
	       $id = $this->db->insert_id();
	       return (isset($id)) ? $id : FALSE;
	   	}


		// register ===================================================chek data=======================================
		public function check_username_exists($username){
			$query = $this->db->get_where('tbl_admin', array('username' => $username));
			if(empty($query->row_array())){
				return true;
			} else {
				return false;
			}
		}

	
		public function check_email_exists($email_admin){
			$query = $this->db->get_where('tbl_admin', array('email_admin' => $email_admin));
			if(empty($query->row_array())){
				return true;
			} else {
				return false;
			}
		}

}