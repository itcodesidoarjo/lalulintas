<?php
	class Mpengaduan extends CI_Model
	{
		public function input_pengaduan($data)
		{
		   $this->db->insert('tbl_pengaduan', $data);
		   $id = $this->db->insert_id();
		 }

		public function dt_pengaduan()
		{
		    $query=$this->db->query("
		    	SELECT * FROM tbl_pengaduan ");
		   	return $query->result();
		}

		public function dt_pengaduan_user($id)
		{
		    $query=$this->db->query("
		    	SELECT * FROM tbl_pengaduan where id_user =".$id);
		   	return $query->result();
		}

		public function konfirmasi($data,$where){
		  	$this->db->where($where);
			$this->db->update('tbl_pengaduan',$data);
		}

		public function dt_pengaduan_detail($id)
		{
		    $query=$this->db->query("
		    	SELECT * FROM tbl_pengaduan a
		    	LEFT JOIN tbl_user b on b.id_user=a.id_user where id_pengaduan =".$id);
		   	return $query->result();
		} 

		public function total($id)
		{
			if (!empty($id)) {
				$where= " WHERE true and id_user = ".$id;
			}else{
				$where = "";
			}
		    $query=$this->db->query("
		    	SELECT COUNT(*) AS total FROM tbl_pengaduan".$where);
		   	return $query->result();
		} 

		public function tot_belumkonfirmasi($id)
		{
			if (!empty($id)) {
				$where= " and id_user =".$id;
			}else{
				$where = "";
			}
		    $query=$this->db->query("
		    	SELECT COUNT(*) AS totbelumkonfirmasi FROM tbl_pengaduan WHERE true and status_pengaduan = 1".$where);
		   	return $query->result();
		} 

		public function tot_konfirmasi($id)
		{
			if (!empty($id)) {
				$where= " and id_user =".$id;
			}else{
				$where = "";
			}
		    $query=$this->db->query("
		    	SELECT COUNT(*) AS totkonfirmasi FROM tbl_pengaduan WHERE true and status_pengaduan = 2".$where);
		   	return $query->result();
		} 

		public function tot_batal($id)
		{
			if (!empty($id)) {
				$where= " and id_user =".$id;
			}else{
				$where = "";
			}
		    $query=$this->db->query("
		    	SELECT COUNT(*) AS totbatal FROM tbl_pengaduan WHERE true and status_pengaduan = 3".$where);
		   	return $query->result();
		}

		public function dt_lokasi()
		{
		    $query=$this->db->query("
		    	SELECT a.`idkecamatan`,kecamatan,iddesa,desa,kodepos
				FROM tbl_kecamatan a
				INNER JOIN tbl_desa b ON b.`idkecamatan` = a.`idkecamatan`");
		   	return $query->result();
		}
 
	}