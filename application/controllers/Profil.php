<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 public function __construct(){
        parent::__construct();
        if($this->session->userdata('status') != "login"){
            redirect(base_url("#home"));
        }
    }

  public function profil($id)
  {
  	$akses = $this->session->userdata('akses');
  	if ($akses == 1) {
		$data['profil'] = $this->mregister->dt_admin($id);
		$this->load->view('header.php');
    	$this->load->view('admin/detail_profiladmin.php',$data);
    	$this->load->view('footer.php');
  	}
  	if ($akses == 2) {
		$data['profil'] = $this->mregister->dt_user($id);

		$this->load->view('header.php');
    	$this->load->view('admin/detail_profil.php',$data);
    	$this->load->view('footer.php');
  	}
  }


  	public function update_admin()
	{
		
	 	$config = [
	        'upload_path' 	=> './asset/img/',
	        'allowed_types' => 'gif|jpg|png|jpeg|bmp',
	        'max_size' 		=> 5000, 
	        'remove_space'	=> TRUE,
	      	];
	    
	    $id 	= $this->session->userdata('ses_id_admin');
	     

      	$this->load->library('upload', $config);
      	if (!$this->upload->do_upload()) //jika gagal upload
      	{
	         $error = array('error' => $this->upload->display_errors()); //tampilkan error

	        $this->session->set_flashdata('error','Foto anda tidak sesui dengan format!');
	       	redirect('profil/profil/'.$id); 

      	}else{		
	      	$file = $this->upload->data();
	     	date_default_timezone_set('Asia/Jakarta');
	     	$jam = date("Y-m-d h:i:s");
	     	
		      $data = [
		       'foto_admin' 		=> $file['file_name'],
		       'nik_admin' 			=> set_value('nik_admin'),
		       'nama_admin'			=> set_value('nama_admin'),
		       'telepon_admin' 		=> set_value('telepon_admin'),
		       'alamat_admin'		=> set_value('alamat_admin'),
		     ];
	     	
	     	$where = array(
				'id_admin' => $id
			);
		      $this->mregister->update_profil($data,$where); 
	          $this->session->set_flashdata('pesan','Berhasil disimpan.');
		      redirect('profil/profil/'.$id); 
		}
	}

	public function update_user()
	{
		
	 	$config = [
	        'upload_path' 	=> './asset/img/',
	        'allowed_types' => 'gif|jpg|png|jpeg|bmp',
	        'max_size' 		=> 5000, 
	        'remove_space'	=> TRUE,
	      	];
	    
	    $id 	= $this->session->userdata('ses_id_user');
	     

      	$this->load->library('upload', $config);
      	if (!$this->upload->do_upload()) //jika gagal upload
      	{
	         $error = array('error' => $this->upload->display_errors()); //tampilkan error

	        $this->session->set_flashdata('error','Foto anda tidak sesui dengan format!');
	       	redirect('profil/profil/'.$id); 

      	}else{		
	      	$file = $this->upload->data();
	     	date_default_timezone_set('Asia/Jakarta');
	     	$jam = date("Y-m-d h:i:s");
	     	
		      $data = [
		       'foto_user' 		=> $file['file_name'],
		       'nik_user' 			=> set_value('nik_user'),
		       'nama_user'			=> set_value('nama_user'),
		       'telepon_user' 		=> set_value('telepon_user'),
		       'alamat_user'		=> set_value('alamat_user'),
		     ];
	     	
	     	$where = array(
				'id_user' => $id
			);
		      $this->mregister->update_profil_user($data,$where); 
	          $this->session->set_flashdata('pesan','Berhasil disimpan.');
		      redirect('profil/profil/'.$id); 
		}
	}
}
