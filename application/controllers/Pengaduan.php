<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaduan extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
        parent::__construct();
        if($this->session->userdata('status') != "login"){
            redirect(base_url("welcome"));
        }
    }
	public function index()
	{
		$akses = $this->session->userdata('akses');
		if ($akses == 1) {
			$data['pengaduan'] = $this->mpengaduan->dt_pengaduan();
		}
		if ($akses == 2) {
			$id = $this->session->userdata('ses_id_user');
			$data['pengaduan'] = $this->mpengaduan->dt_pengaduan_user($id);
		}
		
		$this->load->view('header');
		$this->load->view('admin/dt_pengaduan',$data);
		$this->load->view('footer');
	}

	public function form_pengaduan()
	{
		$data['tbl_rambu'] = $this->mrambu->dt_rambu();
		$data['tbl_lokasi'] = $this->mpengaduan->dt_lokasi();

		$this->load->view('header');
		$this->load->view('admin/form_pengaduan',$data);
		$this->load->view('footer');
	}

	public function input_tambah()
	{
		$idkecamatan	= $this->input->post('idkecamatan');
		$id_rambu	= $this->input->post('id_rambu');
		
		if (empty($idkecamatan) || empty($id_rambu)) {
			 $this->session->set_flashdata('error','Rambu atau Lokasi belum diisi!');
	       	 redirect('pengaduan/form_pengaduan'); 
		}else{
		 	$config = [
		        'upload_path' 	=> './asset/img/',
		        'allowed_types' => 'gif|jpg|png|jpeg|bmp',
		        'max_size' 		=> 5000, 
		        'remove_space'	=> TRUE,
		      	];

	      	$this->load->library('upload', $config);
	      	if (!$this->upload->do_upload()) //jika gagal upload
	      	{
		         $error = array('error' => $this->upload->display_errors()); //tampilkan error

		        $this->session->set_flashdata('error','Foto anda tidak sesui dengan format!');
		       	redirect('pengaduan/form_pengaduan'); 

	      	}else{		
		      	$file 	= $this->upload->data();
		     	date_default_timezone_set('Asia/Jakarta');
		     	$jam 	= date("Y-m-d h:i:s");
		     	$id 	= empty($this->session->userdata('ses_id_user'))?$this->session->userdata('ses_id_admin'):$this->session->userdata('ses_id_user');

			      $data = [
			       'foto_pengaduan' 	=> $file['file_name'],
			       'id_user' 			=> $id,
			       'isi_pengaduan'		=> set_value('isi_pengaduan'),
			       'alamat_pengaduan'	=> set_value('alamat_pengaduan'),
			       'idkecamatan'		=> set_value('iddesa'),
			       'id_rambu'			=> set_value('id_rambu'),
			       'tanggal_pengaduan' 	=> $jam,
			     ];
		     	
			      $this->mpengaduan->input_pengaduan($data); 
		          $this->session->set_flashdata('pesan','Berhasil disimpan.');
			      redirect('pengaduan'); 
			}
		}
	}
	public function konfirmasi($id)
	{
		$data = [
           'status_pengaduan' 		=> 2,
     	];
      	$where = array(
			'id_pengaduan' => $id
		);

		$konfirmasi = $this->mpengaduan->konfirmasi($data,$where);
		$this->session->set_flashdata('pesan','Data anggota berhasil di konfirmasi');
		redirect('pengaduan');
		
	}

	public function batal($id)
	{
		$data = [
           'status_pengaduan' 		=> 3,
     	];
      	$where = array(
			'id_pengaduan' => $id
		);

		$konfirmasi = $this->mpengaduan->konfirmasi($data,$where);
		$this->session->set_flashdata('pesan','Data anggota berhasil di batalkan');
		redirect('pengaduan');
		
	}

	public function detail($id)
	{
		$data['pengaduan'] = $this->mpengaduan->dt_pengaduan_detail($id);
		$this->load->view('header');
		$this->load->view('admin/detail_pelaporan',$data);
		$this->load->view('footer');
	}
}
