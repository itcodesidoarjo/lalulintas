<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rambu extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
        parent::__construct();
        if($this->session->userdata('status') != "login"){
            redirect(base_url("welcome"));
        }
    }
	public function index()
	{
		$data['rambu'] = $this->mrambu->dt_rambu();
		$this->load->view('header');
		$this->load->view('admin/dt_rambu',$data);
		$this->load->view('footer');
	}
	public function form_tambah()
	{
		$this->load->view('header');
		$this->load->view('admin/form_rambu');
		$this->load->view('footer');
	}

	public function input_tambah()
	{
		
		$jenis	= $this->input->post('jenis_rambu');

	 	$config = [
	        'upload_path' 	=> './asset/img/',
	        'allowed_types' => 'gif|jpg|png|jpeg|bmp',
	        'max_size' 		=> 5000, 
	        'remove_space'	=> TRUE,
	      	];

      	$this->load->library('upload', $config);
      	if (!$this->upload->do_upload()) //jika gagal upload
      	{
	         $error = array('error' => $this->upload->display_errors()); //tampilkan error

	        $this->session->set_flashdata('error','Foto anda tidak sesui dengan format!');
	       	redirect('rambu/form_tambah'); 

      	}else{	
      		if (!empty($jenis)) {

		      	$file = $this->upload->data();
		     	date_default_timezone_set('Asia/Jakarta');
		     	$jam = date("Y-m-d h:i:s");
		     	
			     $data = [
			       'logo_rambu' 			=> $file['file_name'],
			       'nama_rambu' 			=> set_value('nama_rambu'),
			       'kondisi_rambu' 			=> set_value('kondisi_rambu'),
			       'jenis_rambu'			=> set_value('jenis_rambu'),
			       'tahun_rambu' 			=> set_value('tahun_rambu'),
			       'keterangan_rambu'		=> set_value('keterangan_rambu'),
			       'tglinput_rambu' 		=> $jam,
			     ];
		     	
			      $this->mrambu->input_rambu($data); 
		          $this->session->set_flashdata('pesan','Berhasil disimpan.');
			      redirect('rambu'); 
			}else{
				$this->session->set_flashdata('error','Jenis rambu belum di isi!');
	       		redirect('rambu/form_tambah'); 
			}
		}
	}

	public function hapus($id)
	{
		$data = [
           'status_rambu' 		=> 2,
     	];
      	$where = array(
			'id_rambu' => $id
		);

		$hapus = $this->mrambu->hapus($data,$where);
		$this->session->set_flashdata('pesan','Data anggota berhasil di hapus');
		redirect('rambu');
		
	}

	public function detail($id)
	{
		$data['rambu'] = $this->mrambu->dt_rambu_detail($id);
		$this->load->view('header');
		$this->load->view('admin/detail_rambu',$data);
		$this->load->view('footer');
	}

	public function edit($id)
	{
		$data['rambu'] = $this->mrambu->dt_rambu_detail($id);
		$this->load->view('header');
		$this->load->view('admin/update_rambu',$data);
		$this->load->view('footer');
	}

	public function input_update()
	{
		$id_rambu	= $this->input->post('id_rambu');

	 	$config = [
	        'upload_path' 	=> './asset/img/',
	        'allowed_types' => 'gif|jpg|png|jpeg|bmp',
	        'max_size' 		=> 5000, 
	        'remove_space'	=> TRUE,
	      	];

      	$this->load->library('upload', $config);
      	if (!$this->upload->do_upload()) //jika gagal upload
      	{
	         $error = array('error' => $this->upload->display_errors()); //tampilkan error

	        $this->session->set_flashdata('error','Foto anda tidak sesui dengan format!');
	       	redirect('rambu/edit/'.$id_rambu); 

      	}else{		
	      	$file = $this->upload->data();
	     	date_default_timezone_set('Asia/Jakarta');
	     	$jam = date("Y-m-d h:i:s");
	     	
		      $data = [
		       'logo_rambu' 			=> $file['file_name'],
		       'nama_rambu' 			=> set_value('nama_rambu'),
		       'kondisi_rambu' 			=> set_value('kondisi_rambu'),
		       'jenis_rambu'			=> set_value('jenis_rambu'),
		       'tahun_rambu' 			=> set_value('tahun_rambu'),
		       'keterangan_rambu'		=> set_value('keterangan_rambu'),
		       'tglinput_rambu' 		=> $jam,
		     ];
	     	
	     	$id 	= set_value('id_rambu');
	     	$where = array(
				'id_rambu' => $id
			);
		      $this->mrambu->update_rambu($data,$where); 
	          $this->session->set_flashdata('pesan','Berhasil disimpan.');
		      redirect('rambu'); 
		}
	}
}
