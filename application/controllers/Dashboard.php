<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
        parent::__construct();
        if($this->session->userdata('status') != "login"){
            redirect(base_url("welcome"));
        }
    }

	public function index()
	{
		
		$id = $this->session->userdata('ses_id_user');
		
		$data['total'] 					= $this->mpengaduan->total($id);
		$data['tot_belumkonfirmasi'] 	= $this->mpengaduan->tot_belumkonfirmasi($id);
		$data['tot_konfirmasi'] 		= $this->mpengaduan->tot_konfirmasi($id);
		$data['tot_batal'] 				= $this->mpengaduan->tot_batal($id);

		$this->load->view('header');
		$this->load->view('admin/dashboard',$data);
		$this->load->view('footer');
	}
}
