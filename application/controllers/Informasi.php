<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Informasi extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
        parent::__construct();
        if($this->session->userdata('status') != "login"){
            redirect(base_url("welcome"));
        }
    }
	public function index()
	{
		$data['rambu'] = $this->mrambu->dt_rambu();
		$this->load->view('header');
		$this->load->view('admin/dt_informasi',$data);
		$this->load->view('footer');
	}
	public function tambah($id)
	{
		$data['id'] = $id;
		$this->load->view('header');
		$this->load->view('admin/form_informasi',$data);
		$this->load->view('footer');
	}

	public function pdf($id)
	{
		$data['id'] = $id;
		$data['rambu'] = $this->mrambu->dt_pdf_rambu($id);
		$this->load->view('header');
		$this->load->view('admin/informasi_pdf',$data);
		$this->load->view('footer');
	}


	public function input_tambah()
	{
		$id_rambu	= $this->input->post('id_rambu');
		
	 	$config = [
	        'upload_path' 	=> './asset/img/',
	        'allowed_types' => 'pdf',
	        'max_size' 		=> 5000, 
	        'remove_space'	=> TRUE,
	      	];

      	$this->load->library('upload', $config);
      	if (!$this->upload->do_upload()) //jika gagal upload
      	{
	         $error = array('error' => $this->upload->display_errors()); //tampilkan error

	        $this->session->set_flashdata('error','File anda tidak sesui dengan format!');
	       	redirect('Informasi/tambah/'.$id_rambu); 

      	}else{		
	      	$file 	= $this->upload->data();

		      $data = [
		       'file_rambu' 		=> $file['file_name'],
		     ];

		     $where = array(
				'id_rambu' => $id_rambu
			);
	     	
		      $this->mrambu->update_rambu($data,$where); 
	          $this->session->set_flashdata('pesan','Berhasil disimpan.');
		      redirect('Informasi'); 
		}
	}
}
