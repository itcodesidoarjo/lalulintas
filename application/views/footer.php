        <div class="footer-copyright-area">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer-copy-right">
                            <p>Copyright &copy; 2019 <a href="https://colorlib.com/wp/templates/">E-Rambu</a> v.1.001</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script   src="https://code.jquery.com/jquery-3.3.1.js"   integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="   crossorigin="anonymous"></script>
   <!-- jquery
        ============================================ -->
    <script src="<?php echo base_url() ?>asset/admin/js/vendor/jquery-1.11.3.min.js"></script>
    <!-- bootstrap JS
        ============================================ -->
    <script src="<?php echo base_url() ?>asset/admin/js/bootstrap.min.js"></script>
    <!-- wow JS
        ============================================ -->
    <script src="<?php echo base_url() ?>asset/admin/js/wow.min.js"></script>
    <!-- price-slider JS
        ============================================ -->
    <script src="<?php echo base_url() ?>asset/admin/js/jquery-price-slider.js"></script>
    <!-- meanmenu JS
        ============================================ -->
    <script src="<?php echo base_url() ?>asset/admin/js/jquery.meanmenu.js"></script>
    <!-- owl.carousel JS
        ============================================ -->
    <script src="<?php echo base_url() ?>asset/admin/js/owl.carousel.min.js"></script>
    <!-- sticky JS
        ============================================ -->
    <script src="<?php echo base_url() ?>asset/admin/js/jquery.sticky.js"></script>
    <!-- scrollUp JS
        ============================================ -->
    <script src="<?php echo base_url() ?>asset/admin/js/jquery.scrollUp.min.js"></script>
    <!-- mCustomScrollbar JS
        ============================================ -->
    <script src="<?php echo base_url() ?>asset/admin/js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="<?php echo base_url() ?>asset/admin/js/scrollbar/mCustomScrollbar-active.js"></script>
    <!-- metisMenu JS
        ============================================ -->
    <script src="<?php echo base_url() ?>asset/admin/js/metisMenu/metisMenu.min.js"></script>
    <script src="<?php echo base_url() ?>asset/admin/js/metisMenu/metisMenu-active.js"></script>
    <!-- data table JS
        ============================================ -->
    <script src="<?php echo base_url() ?>asset/admin/js/data-table/bootstrap-table.js"></script>
    <script src="<?php echo base_url() ?>asset/admin/js/data-table/tableExport.js"></script>
    <script src="<?php echo base_url() ?>asset/admin/js/data-table/data-table-active.js"></script>
    <script src="<?php echo base_url() ?>asset/admin/js/data-table/bootstrap-table-editable.js"></script>
    <script src="<?php echo base_url() ?>asset/admin/js/data-table/bootstrap-editable.js"></script>
    <script src="<?php echo base_url() ?>asset/admin/js/data-table/bootstrap-table-resizable.js"></script>
    <script src="<?php echo base_url() ?>asset/admin/js/data-table/colResizable-1.5.source.js"></script>
    <script src="<?php echo base_url() ?>asset/admin/js/data-table/bootstrap-table-export.js"></script>
    <!--  editable JS
        ============================================ -->
    <script src="<?php echo base_url() ?>asset/admin/js/editable/jquery.mockjax.js"></script>
    <script src="<?php echo base_url() ?>asset/admin/js/editable/mock-active.js"></script>
    <script src="<?php echo base_url() ?>asset/admin/js/editable/select2.js"></script>
    <script src="<?php echo base_url() ?>asset/admin/js/editable/moment.min.js"></script>
    <script src="<?php echo base_url() ?>asset/admin/js/editable/bootstrap-datetimepicker.js"></script>
    <script src="<?php echo base_url() ?>asset/admin/js/editable/bootstrap-editable.js"></script>
    <script src="<?php echo base_url() ?>asset/admin/js/editable/xediable-active.js"></script>
    <script src="<?php echo base_url() ?>asset/admin/js/sparkline/jquery.sparkline.min.js"></script>
    <script src="<?php echo base_url() ?>asset/admin/js/sparkline/jquery.charts-sparkline.js"></script>
    <!-- calendar JS
        ============================================ -->
    <script src="<?php echo base_url() ?>asset/admin/js/calendar/moment.min.js"></script>
    <script src="<?php echo base_url() ?>asset/admin/js/calendar/fullcalendar.min.js"></script>
    <script src="<?php echo base_url() ?>asset/admin/js/calendar/fullcalendar-active.js"></script>
    <!-- Chart JS
        ============================================ -->
    <script src="<?php echo base_url() ?>asset/admin/js/chart/jquery.peity.min.js"></script>
    <script src="<?php echo base_url() ?>asset/admin/js/peity/peity-active.js"></script>
    <!-- tab JS
        ============================================ -->
    <script src="<?php echo base_url() ?>asset/admin/js/tab.js"></script>
    <!-- plugins JS
        ============================================ -->
    <script src="<?php echo base_url() ?>asset/admin/js/plugins.js"></script>
    <!-- main JS
        ============================================ -->
    <script src="<?php echo base_url() ?>asset/admin/js/main.js"></script>
     <!-- summernote JS
        ============================================ -->
    <script src="<?php echo base_url() ?>asset/admin/js/summernote/summernote.min.js"></script>
    <script src="<?php echo base_url() ?>asset/admin/js/summernote/summernote-active.js"></script>

    <!-- touchspin JS
        ============================================ -->
    <script src="<?php echo base_url() ?>asset/admin/js/touchspin/jquery.bootstrap-touchspin.min.js"></script>
    <script src="<?php echo base_url() ?>asset/admin/js/touchspin/touchspin-active.js"></script>
    <!-- colorpicker JS
        ============================================ -->
    <script src="<?php echo base_url() ?>asset/admin/js/colorpicker/jquery.spectrum.min.js"></script>
    <script src="<?php echo base_url() ?>asset/admin/js/colorpicker/color-picker-active.js"></script>
    <!-- datapicker JS
        ============================================ -->
    <script src="<?php echo base_url() ?>asset/admin/js/datapicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url() ?>asset/admin/js/datapicker/datepicker-active.js"></script>
    <!-- input-mask JS
        ============================================ -->
    <script src="<?php echo base_url() ?>asset/admin/js/input-mask/jasny-bootstrap.min.js"></script>
    <!-- chosen JS
        ============================================ -->
    <script src="<?php echo base_url() ?>asset/admin/js/chosen/chosen.jquery.js"></script>
    <script src="<?php echo base_url() ?>asset/admin/js/chosen/chosen-active.js"></script>
    <!-- select2 JS
        ============================================ -->
    <script src="<?php echo base_url() ?>asset/admin/js/select2/select2.full.min.js"></script>
    <script src="<?php echo base_url() ?>asset/admin/js/select2/select2-active.js"></script>
    <!-- ionRangeSlider JS
        ============================================ -->
    <script src="<?php echo base_url() ?>asset/admin/js/ionRangeSlider/ion.rangeSlider.min.js"></script>
    <script src="<?php echo base_url() ?>asset/admin/js/ionRangeSlider/ion.rangeSlider.active.js"></script>
    <!-- rangle-slider JS
        ============================================ -->
    <script src="<?php echo base_url() ?>asset/admin/js/rangle-slider/jquery-ui-1.10.4.custom.min.js"></script>
    <script src="<?php echo base_url() ?>asset/admin/js/rangle-slider/jquery-ui-touch-punch.min.js"></script>
    <script src="<?php echo base_url() ?>asset/admin/js/rangle-slider/rangle-active.js"></script>
    <!-- knob JS
        ============================================ -->
    <script src="<?php echo base_url() ?>asset/admin/js/knob/jquery.knob.js"></script>
    <script src="<?php echo base_url() ?>asset/admin/js/knob/knob-active.js"></script>
     <!-- notification JS
        ============================================ -->
    <script src="<?php echo base_url() ?>asset/admin/js/notifications/Lobibox.js"></script>
    <script src="<?php echo base_url() ?>asset/admin/js/notifications/notification-active.js"></script>

       <!-- pdf JS
        ============================================ -->
    <script src="<?php echo base_url() ?>asset/admin/js/pdf/jquery.media.js"></script>
    <script src="<?php echo base_url() ?>asset/admin/js/pdf/pdf-active.js"></script>
    
    <script type="text/javascript" src="<?php echo base_url(); ?>asset/admin/hhh/datatables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>asset/admin/hhh/datatables/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>asset/admin/hhh/custom/js/script.js"></script>
</body>

</html>