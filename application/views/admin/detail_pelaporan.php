 <br>
</div>
 <?php foreach ($pengaduan as $key):?>
<!-- Static Table Start -->
<div class="data-table-area mg-tb-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="sparkline13-list">
                    <div class="sparkline13-hd">
                        <div class="main-sparkline13-hd">
                            <h1>Detail Pengaduan <span class="table-project-n"></span> </h1>
                        </div>
                    </div>
                    <div class="sparkline13-graph">
                        <div class="datatable-dashv1-list custom-datatable-overright">
                           <!-- Single pro tab start-->
                            <div class="single-product-tab-area mg-t-15 mg-b-30">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                                            <div id="myTabContent1" class="tab-content">
                                                <div class="product-tab-list tab-pane fade active in" id="single-tab1">
                                                    <img src="<?php echo base_url().'asset/img/'.$key->foto_pengaduan;?>" alt="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                                            <div class="single-product-details res-pro-tb">
                                                <h1>Pelapor : <?= $key->username;?></h1>
                                                <div class="single-pro-price">
                                                    <span class="single-regular"></span>
                                                </div>
                                                <div class="single-pro-size">
                                                    <h6>Tanggal Lapor :</h6>
                                                    <p><?=date('d M Y h:i:s', strtotime($key->tanggal_pengaduan));?></p>
                                                </div>
                                                <?php
                                                if ($key->status_pengaduan==1) :?>
                                                   <span class="label label-warning">Belum Dikonfimasi</span>
                                                <?php endif; 
                                                 if ($key->status_pengaduan==2) :?>
                                                   <span class="label label-success">Sudah di Konfirmasi</span>
                                                <?php endif; 
                                                 if ($key->status_pengaduan==3) :?>
                                                   <span class="label label-danger">Batal</span>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Single pro tab End-->
                            <!-- Single pro tab review Start-->
                            <div class="single-pro-review-area mt-t-30 mg-b-15">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <ul id="myTab" class="tab-review-design">
                                                <li class="active"><a href="#description">description (isi)</a></li>
                                            </ul>
                                            <div id="myTabContent" class="tab-content">
                                                <div class="product-tab-list product-details-ect tab-pane fade active in" id="description">
                                                    <div class="row">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <div class="review-content-section">
                                                                <p><?= $key->isi_pengaduan;?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                        <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

