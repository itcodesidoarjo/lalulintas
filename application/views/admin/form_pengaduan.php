<div class="review-tab-pro-inner">
    <ul id="myTab3" class="tab-review-design">
    <?php if($this->session->flashdata('pesan')): ?>
            <div class="alert alert-success alert-success-style1 alert-success-stylenone">
                <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                        <span class="icon-sc-cl" aria-hidden="true">×</span>
                    </button>
                <i class="fa fa-check adminpro-checked-pro admin-check-sucess admin-check-pro-none" aria-hidden="true"></i>
                <p class="message-alert-none"><?=$this->session->flashdata('pesan')?></p>
            </div>
        <?php endif; ?>
         <?php if($this->session->flashdata('error')): ?>
            <div class="alert alert-danger alert-danger-style1 alert-danger-stylenone">
                <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                        <span class="icon-sc-cl" aria-hidden="true">×</span>
                    </button>
                <i class="fa fa-check adminpro-checked-pro admin-check-sucess admin-check-pro-none" aria-hidden="true"></i>
                <p class="message-alert-none"><?=$this->session->flashdata('error')?></p>
            </div>
        <?php endif; ?>
    <li class="active"><a href="#description"><i class="fa fa-pencil" aria-hidden="true"></i>Pengaduan</a></li>
    </ul>
        <div id="myTabContent" class="tab-content custom-product-edit">
            <div class="product-tab-list tab-pane fade active in" id="description">
                <div class="row">
                    <?php echo form_open_multipart('pengaduan/input_tambah'); ?>
                    
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="review-content-section">
                             <div class="chosen-select-single mg-b-20" >
                                <select name="id_rambu"  class="chosen-select" tabindex="-1"> 
                                    <option value="">Pilih Rambu</option>
                                    <?php foreach ($tbl_rambu as $key) {?>
                                        <option value="<?= $key->id_rambu?>"><?= $key->nama_rambu." | ".strtoupper($key->jenis_rambu)?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="input-group custom-go-button">
                                <input type="text" id="kecamatan" placeholder="Cari Lokasi" readonly required class="form-control">
                                <input type="hidden" id="idkecamatan" name="idkecamatan" placeholder="Cari Lokasi" readonly required class="form-control">
                                <input type="hidden" id="iddesa" name="iddesa" placeholder="Cari Lokasi" readonly required class="form-control">
                                <span class="input-group-btn"><a class="btn btn-primary"href="#" data-toggle="modal" data-target="#cari"><i class="fa fa-search" aria-hidden="true"></i></a></span>
                            </div>
                            <br>
                             <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-qrcode" aria-hidden="true"></i></span>
                                <input type="text" id="desa" readonly class="form-control" required="" placeholder="Desa">
                            </div>
                             <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-qrcode" aria-hidden="true"></i></span>
                                <input type="text" id="kodepos" readonly class="form-control" required="" placeholder="Kode Pos">
                            </div>

                             <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-sticky-note " aria-hidden="true"></i></span>
                                <textarea class="form-control" name="alamat_pengaduan" placeholder="Alamat Lengkap"></textarea> 
                            </div>
                             <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-sticky-note " aria-hidden="true"></i></span>
                                <textarea class="form-control" name="isi_pengaduan" placeholder="Isi"></textarea> 
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="review-content-section">
                            <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-file-image-o" aria-hidden="true"></i></span>
                                <input type="file" name="userfile" id="file" class="form-control" required="" placeholder="Logo" onchange="return fileValidation()">
                            </div>
                            <div id="imagePreview"></div>
                        </div>
                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="text-center mg-b-pro-edt custom-pro-edt-ds">
                            <button type="submit" class="btn btn-primary waves-effect waves-light m-r-10">Simpan
                                </button>
                            <a href="<?php echo base_url() ?>pengaduan" type="button" class="btn btn-warning waves-effect waves-light">Batal
                                </a>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
</div>


<div class="modal fade" id="cari" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Cari Rambu</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive" style="width:96%;margin:auto;">
                    <table class="table table-bordered table-hover" id="manageMemberTable">
                         <thead>
                            <tr>
                                <th data-field="id">No</th>
                                <th>Kecamatan</th>
                                <th>Desa</th>
                                <th>Kode Pos</th>
                            </tr>
                        </thead>
                        <tbody>
                             <?php 
                            $no=1;
                            foreach($tbl_lokasi as $key) : ?>
                            <tr onclick="pilih(<?php echo $key->iddesa;?>)" style="cursor:pointer;">
                                <td><?=$no++?></td>
                                <td><?=$key->kecamatan;?></td>
                                <td><?=strtoupper($key->desa);?></td>
                                <td><?=$key->kodepos;?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

    <script type="text/javascript">
            function pilih(id){
                <?php 
                    $sql=$this->db->query("
                        SELECT a.`idkecamatan`,kecamatan,iddesa,desa,kodepos
                        FROM tbl_kecamatan a
                        INNER JOIN tbl_desa b ON b.`idkecamatan` = a.`idkecamatan`");
                     foreach($sql->result() as $s){  
                ?>
                if (id == <?php echo $s->iddesa; ?>) {
                    document.getElementById("kecamatan").value="<?php echo $s->kecamatan;?>";
                    document.getElementById("idkecamatan").value="<?php echo $s->idkecamatan;?>";
                    document.getElementById("iddesa").value="<?php echo $s->iddesa;?>";
                    document.getElementById("desa").value="<?php echo $s->desa;?>";
                    document.getElementById("kodepos").value="<?php echo $s->kodepos;?>";
                    $("#cari").modal('hide');
                }
                <?php } ?>
               
            }
        </script>
         <script type="text/javascript">
            function fileValidation(){
            var fileInput = document.getElementById('file');
            var filePath = fileInput.value;
            var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
            if(!allowedExtensions.exec(filePath)){
                alert('Format upload file hanya .jpeg/.jpg/.png/.gif.!');
                fileInput.value = '';
                return false;
            }else{
                //Image preview
                if (fileInput.files && fileInput.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        document.getElementById('imagePreview').innerHTML = '<img  src="'+e.target.result+'"  height="200px" width="200px" style="padding-bottom:7cm"/>';
                    };
                    reader.readAsDataURL(fileInput.files[0]);
                }
            }
        }
        </script>   
