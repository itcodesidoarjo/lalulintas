
            <!-- Mobile Menu end -->
            <div class="breadcome-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="breadcome-list">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="breadcome-heading">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <ul class="breadcome-menu">
                                            <li><a href="#">Home</a> <span class="bread-slash">/</span>
                                            </li>
                                            <li><span class="bread-blod">Dashboard</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-admin container-fluid res-mg-t-15">
            <div class="row admin text-center">
                <div class="col-md-12">
                    <div class="row">
                        <?php foreach ($total as $key): ?>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="admin-content analysis-progrebar-ctn">
                                <h4 class="text-left text-uppercase"><b>Total Laporan</b></h4>
                                <div class="row vertical-center-box vertical-center-box-tablet">
                                    <div class="col-xs-3 mar-bot-15 text-left">
                                        <label class="label bg-green"><?=$key->total?><i class="fa fa-level-up" aria-hidden="true"></i></label>
                                    </div>
                                    <div class="col-xs-9 cus-gh-hd-pro">
                                        <!-- <h2 class="text-right no-margin"><?=$key->total?></h2> -->
                                    </div>
                                </div>
                                <div class="progress progress-mini">
                                    <div style="width:<?=$key->total?>%;" class="progress-bar bg-green"></div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach ?>
                        <?php foreach ($tot_batal as $key): ?>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-bottom:1px;">
                            <div class="admin-content analysis-progrebar-ctn res-mg-t-30">
                                <h4 class="text-left text-uppercase"><b> Dibatalakan</b></h4>
                                <div class="row vertical-center-box vertical-center-box-tablet">
                                    <div class="text-left col-xs-3 mar-bot-15">
                                        <label class="label bg-red"><?=$key->totbatal?><i class="fa fa-level-down" aria-hidden="true"></i></label>
                                    </div>
                                    <div class="col-xs-9 cus-gh-hd-pro">
                                        <!-- <h2 class="text-right no-margin"><?=$key->totbelumkonfirmasi?></h2> -->
                                    </div>
                                </div>
                                <div class="progress progress-mini">
                                    <div style="width: <?=$key->totbatal?>%;" class="progress-bar progress-bar-danger bg-red"></div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach ?>
                        <?php foreach ($tot_konfirmasi as $key): ?>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="admin-content analysis-progrebar-ctn res-mg-t-30">
                                <h4 class="text-left text-uppercase"><b> Dikonfirmasi</b></h4>
                                <div class="row vertical-center-box vertical-center-box-tablet">
                                    <div class="text-left col-xs-3 mar-bot-15">
                                        <label class="label bg-blue"><?=$key->totkonfirmasi?> <i class="fa fa-level-up" aria-hidden="true"></i></label>
                                    </div>
                                    <div class="col-xs-9 cus-gh-hd-pro">
                                        <!-- <h2 class="text-right no-margin"><?=$key->totkonfirmasi?></h2> -->
                                    </div>
                                </div>
                                <div class="progress progress-mini">
                                    <div style="width: <?=$key->totkonfirmasi?>%;" class="progress-bar bg-blue"></div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach ?>
                        <?php foreach ($tot_belumkonfirmasi as $key): ?>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="admin-content analysis-progrebar-ctn res-mg-t-30">
                                <h4 class="text-left text-uppercase"><b> belum Konfirmasi</b></h4>
                                <div class="row vertical-center-box vertical-center-box-tablet">
                                    <div class="text-left col-xs-3 mar-bot-15">
                                        <label class="label bg-purple"><?=$key->totbelumkonfirmasi?><i class="fa fa-level-up" aria-hidden="true"></i></label>
                                    </div>
                                    <div class="col-xs-9 cus-gh-hd-pro">
                                        <!-- <h2 class="text-right no-margin"><?=$key->totbatal?></h2> -->
                                    </div>
                                </div>
                                <div class="progress progress-mini">
                                    <div style="width: <?=$key->totbelumkonfirmasi?>%;" class="progress-bar bg-purple"></div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach ?>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="author-area-pro">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="personal-info-wrap">
                            <div class="widget-head-info-box">
                                <div class="persoanl-widget-hd">
                                    <h2> <?= ($this->session->userdata('akses') == 1)?"DATA PENGADUAN":"LAPOR"?></h2>
                                </div>
                                <a href="<?php echo base_url(($this->session->userdata('akses') == 1)?'pengaduan':'pengaduan/form_pengaduan')?>"><img style="max-width:150px" src="<?php echo base_url(); ?>asset/admin/img/up.png" class="img-circle circle-border m-b-md" alt="profile"></a>
                            </div>
                          
                        </div>
                    </div>
                     <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    </div>
                </div>
            </div>
        </div>
        <div class="product-sales-area mg-tb-30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-sales-chart">
                            <div class="portlet-title">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="caption pro-sl-hd">
                                            <span class="caption-subject text-uppercase"><b>Road Inventory | kab. Sidoarjo</b></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="actions graph-rp">
                                            <?php if ($this->session->userdata('akses') == 1):?>
                                            <a href="<?php echo base_url('informasi'); ?>" class="btn btn-dark-blue btn-circle active tip-top" data-toggle="tooltip" title="Upload">
                                                    <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                </a>
                                            <?php endif; ?>
                                            <?php if ($this->session->userdata('akses') != 1):?>
                                            <a href="<?php echo base_url('informasi'); ?>" class="btn btn-dark btn-circle active tip-top" data-toggle="tooltip" title="Refresh">
                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                            </a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="extra-area-chart"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>