<div class="review-tab-pro-inner">
    <ul id="myTab3" class="tab-review-design">
    <?php if($this->session->flashdata('pesan')): ?>
            <div class="alert alert-success alert-success-style1 alert-success-stylenone">
                <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                        <span class="icon-sc-cl" aria-hidden="true">×</span>
                    </button>
                <i class="fa fa-check adminpro-checked-pro admin-check-sucess admin-check-pro-none" aria-hidden="true"></i>
                <p class="message-alert-none"><?=$this->session->flashdata('pesan')?></p>
            </div>
        <?php endif; ?>
         <?php if($this->session->flashdata('error')): ?>
            <div class="alert alert-danger alert-danger-style1 alert-danger-stylenone">
                <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                        <span class="icon-sc-cl" aria-hidden="true">×</span>
                    </button>
                <i class="fa fa-check adminpro-checked-pro admin-check-sucess admin-check-pro-none" aria-hidden="true"></i>
                <p class="message-alert-none"><?=$this->session->flashdata('error')?></p>
            </div>
        <?php endif; ?>
    <li class="active"><a href="#description"><i class="fa fa-pencil" aria-hidden="true"></i>Tambah Data Rambu</a></li>
    </ul>
        <div id="myTabContent" class="tab-content custom-product-edit">
            <div class="product-tab-list tab-pane fade active in" id="description">
                <div class="row">
                    <?php echo form_open_multipart('rambu/input_tambah'); ?>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="review-content-section">
                            <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-book" aria-hidden="true"></i></span>
                                <input type="text" name="nama_rambu" class="form-control" required="" placeholder="Nama Rambu">
                            </div>
                            <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-qrcode" aria-hidden="true"></i></span>
                                <input type="text" name="kondisi_rambu" class="form-control" required="" placeholder="Kondisi">
                            </div>
                          
                             <div class="chosen-select-single mg-b-20" >
                                <select name="jenis_rambu"  class="chosen-select" tabindex="-1">
                                    <option value="">Pilih Jenis</option>
                                    <option value="konvensional">Konvensional</option>
                                    <option value="elektronik">Elektronik</option>
                                </select>
                            </div>
                            <div class="form-group data-custon-pick" id="data_3">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text"  name="tahun_rambu" placeholder="Tahun Pembuatan" class="form-control" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="review-content-section">
                             <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-sticky-note " aria-hidden="true"></i></span>
                                <textarea class="form-control" name="keterangan_rambu" placeholder="Keterangan"></textarea> 
                            </div>
                            <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-file-image-o" aria-hidden="true"></i></span>
                                <input type="file" id="file" name="userfile" class="form-control" required="" placeholder="Logo" onchange="return fileValidation()">
                            </div>
                            <div id="imagePreview"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="text-center mg-b-pro-edt custom-pro-edt-ds">
                            <button type="submit" class="btn btn-primary waves-effect waves-light m-r-10">Simpan
                                </button>
                            <a href="<?php echo base_url() ?>rambu" type="button" class="btn btn-warning waves-effect waves-light">Batal
                                </a>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
</div>
  <script type="text/javascript">
            function fileValidation(){
            var fileInput = document.getElementById('file');
            var filePath = fileInput.value;
            var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
            if(!allowedExtensions.exec(filePath)){
                alert('Format upload file hanya .jpeg/.jpg/.png/.gif.!');
                fileInput.value = '';
                return false;
            }else{
                //Image preview
                if (fileInput.files && fileInput.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        document.getElementById('imagePreview').innerHTML = '<img  src="'+e.target.result+'"  height="200px" width="200px" style="padding-bottom:7cm" />';
                    };
                    reader.readAsDataURL(fileInput.files[0]);
                }
            }
        }
        </script>   

