<br>
</div>
<!-- Static Table Start -->
<div class="data-table-area mg-tb-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="sparkline13-list">
                    <div class="sparkline13-hd">
                        <div class="main-sparkline13-hd">
                            <h1>Detail Rambu <span class="table-project-n"></span> </h1>
                        </div>
                    </div>
                    <div class="sparkline13-graph">
                        <div class="datatable-dashv1-list custom-datatable-overright">
                           <!-- Single pro tab start-->
                             <?php foreach ($rambu as $key):?>
                                    <div class="container">
                                        <div class="row">
                                            <div class=" col-md-5 ">
                                                <div id="myTabContent1" class="tab-content">
                                                    <div class="product-tab-list tab-pane fade active in" id="single-tab1">
                                                        <img src="<?php echo base_url().'asset/img/'.$key->logo_rambu;?>" alt="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-7 ">
                                                <div class="single-product-details res-pro-tb">
                                                    <h1><?= $key->nama_rambu ?></h1>
                                                    <span class="single-pro-star"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
                                                    <div class="single-pro-size">
                                                        <h6>Jenis</h6>
                                                        <p><?= $key->jenis_rambu ?></p>
                                                    </div>


                                                    <div class="color-quality-pro">
                                                        <div class="color-quality-details">
                                                            <h5>Tahun</h5>
                                                            <p><?= $key->tahun_rambu ?></p>
                                                        </div>
                                                        <div class="clear"></div>
                                                        <div class="single-pro-button">
                                                            <div class="pro-button">
                                                                <a href="#"><?= $key->kondisi_rambu ?></a>
                                                            </div>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="single-pro-cn">
                                                        <h5>KETERANGAN</h5>
                                                        <div class="row">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="user-comment">
                                                                    <div class="comment-details">
                                                                        <p><?= $key->keterangan_rambu?></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                             <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>