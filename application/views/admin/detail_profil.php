 
<br>

</div>
<!-- Static Table Start -->
<div class="data-table-area mg-tb-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="sparkline13-list">
                    <div class="sparkline13-hd">
                        <div class="main-sparkline13-hd">
                            <h1>Detail Profile <span class="table-project-n"></span> </h1>
                        </div>
                    </div>
                    <div class="sparkline13-graph">
                        <div class="datatable-dashv1-list custom-datatable-overright">
                           <!-- Single pro tab start-->
                            <?php foreach ($profil as $key):?>
                                <div class="single-product-tab-area mg-t-15 mg-b-30">
                                    <div class="container-fluid">
                                        <div class="row">
                                        <?php if($this->session->flashdata('pesan')): ?>
                                            <div class="alert alert-success alert-success-style1 alert-success-stylenone">
                                                <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                                                        <span class="icon-sc-cl" aria-hidden="true">×</span>
                                                    </button>
                                                <i class="fa fa-check adminpro-checked-pro admin-check-sucess admin-check-pro-none" aria-hidden="true"></i>
                                                <p class="message-alert-none"><?=$this->session->flashdata('pesan')?></p>
                                            </div>
                                        <?php endif; ?>
                                         <?php if($this->session->flashdata('error')): ?>
                                            <div class="alert alert-danger alert-danger-style1 alert-danger-stylenone">
                                                <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                                                        <span class="icon-sc-cl" aria-hidden="true">×</span>
                                                    </button>
                                                <i class="fa fa-check adminpro-checked-pro admin-check-sucess admin-check-pro-none" aria-hidden="true"></i>
                                                <p class="message-alert-none"><?=$this->session->flashdata('error')?></p>
                                            </div>
                                        <?php endif; ?>
                                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                                                <div id="myTabContent1" class="tab-content">
                                                    <div class="product-tab-list tab-pane fade active in" id="single-tab1">
                                                        <img  src="<?php echo base_url(). 'asset/img/'.$key->foto_user;?>" alt="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                                                <div class="single-product-details res-pro-tb">
                                                    <h1><?= $key->username;?></h1>
                                                    <span class="single-pro-star"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
                                                    <div class="single-pro-price">
                                                        <span class="single-regular"><?= $key->nama_user;?></span>
                                                    </div>
                                                    <div class="single-pro-size">
                                                        <h6>Email</h6>
                                                        <p><?= $key->email_user;?></p>
                                                    </div>
                                                    <div class="color-quality-pro">
                                                        <div class="color-quality-details">
                                                            <h5>Telpone</h5>
                                                            <p><?= $key->telepon_user;?></p>
                                                        </div>
                                                        <div class="clear"></div>
                                                        
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="single-pro-cn">
                                                        <h3>ALAMAT</h3>
                                                        <p><?= $key->alamat_user;?></p>
                                                    </div>
                                                     <a data-toggle="modal" data-target="#tambah-data" class="btn btn-primary"> <i class="fa fa-pencil"></i> Edit</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <!-- modal tambah kelas -->
  <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="tambah-data" class="modal fade">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                  <h4 class="modal-title">Edit Profil</h4>
              </div>
                <?php echo form_open_multipart(base_url('profil/update_user')); ?>
                <div class="modal-body">
                    
                        <div class="form-group">
                            <label>No. Identitas</label>
                            <input type="text" name="nik_user" value=" <?=$key->nik_user;?> " class="form-control span12">
                        </div>
                         <div class="form-group">
                            <label>Nama</label>
                            <input type="hidden" name="id_user" value=" <?=$key->id_user;?>" class="form-control span12">
                            <input type="text" name="nama_user" value=" <?=$key->nama_user;?>" class="form-control span12">
                        </div>

                        <div class="form-group">
                            <label>No. Tlpn</label>
                            <input type="text" name="telepon_user" value=" <?=$key->telepon_user;?> " class="form-control span12">
                        </div>
                         <div class="form-group">
                            <label>Alamat</label>
                            <input type="text" name="alamat_user" value=" <?=$key->alamat_user;?>" class="form-control span12">
                        </div>

                         <div class="form-group">
                            <label>Foto Profil</label>
                            <input type="file" id="file" name="userfile" required="" onchange="return fileValidation()"  />
                            <br>
                            <div id="imagePreview"></div>
                          </div>

                         <h5 style="color: red;"><?php echo validation_errors(); ?></h5>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" type="submit"> Simpan&nbsp;</button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal"> Batal</button>
                    </div>
               <?php echo form_close(); ?>
              </div>
          </div>
      </div>

   <script type="text/javascript">
        function fileValidation(){
        var fileInput = document.getElementById('file');
        var filePath = fileInput.value;
        var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
        if(!allowedExtensions.exec(filePath)){
            alert('Format upload file hanya .jpeg/.jpg/.png/.gif.!');
            fileInput.value = '';
            return false;
        }else{
            //Image preview
            if (fileInput.files && fileInput.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    document.getElementById('imagePreview').innerHTML = '<img  src="'+e.target.result+'"  height="200px" width="200px"/>';
                };
                reader.readAsDataURL(fileInput.files[0]);
            }
        }
    }
    </script> 
            <?php endforeach; ?>
</div>