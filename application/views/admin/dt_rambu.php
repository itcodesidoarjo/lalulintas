 <?php function limit_words($string, $word_limit){
        $words = explode(" ",$string);
        return implode(" ",array_splice($words,0,$word_limit));
    }
    ?>
<div class="breadcome-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcome-list single-page-breadcome">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="breadcome-heading">
                                   
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <ul class="breadcome-menu">
                                    <li><a href="#">Data</a> <span class="bread-slash"></span>
                                    </li>
                                    <li><span class="bread-blod">Rambu</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Static Table Start -->
<div class="data-table-area mg-tb-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="sparkline13-list">
                    <div class="sparkline13-hd">
                        <div class="main-sparkline13-hd">
                            <h1>Data Rambu <span class="table-project-n"></span> </h1>
                            <?php if ($this->session->userdata('akses') == 1):?>
                            <a href="<?php echo base_url('rambu/form_tambah');?>" type="submit" class="btn btn-success"><i class="fa fa-plus"></i>  Tambah</a>
                        <?php endif; ?>
                        </div>

                    </div>

                    <div class="sparkline13-graph">
                        <div class="datatable-dashv1-list custom-datatable-overright">
                            <div id="toolbar">
                            </div>
                            <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="false" data-show-pagination-switch="false" data-show-refresh="false" data-key-events="false" data-show-toggle="false" data-resizable="false" data-cookie="false"
                                data-cookie-id-table="saveId" data-show-export="false" data-click-to-select="false" data-toolbar="#toolbar">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Logo</th>
                                        <th>Nama Rambu</th>
                                        <th>Jenis</th>
										<th>Kondisi</th>
                                        <th>Tahun</th>
										<th>Keterangan</th>
                                        <th>Action</th>
                                    </tr>

                                </thead>
                                 <tbody>
                                    <?php 
                                    $no=1;
                                    foreach($rambu as $value) : ?>
                                    <tr>
                                        <td><?=$no++?></td>
                                        <td>
                                            <img style="min-height:200px; max-height:200px; min-width:200px; max-width:200px;" class="img-thumbnail" src="<?php echo base_url() . 'asset/img/'.$value->logo_rambu?>"/>
                                        </td>
                                        <td><?=$value->nama_rambu;?></td>
                                        <td><?=strtoupper($value->jenis_rambu);?></td>
                                        <td><?=$value->kondisi_rambu;?></td>
                                        <td><?=$value->tahun_rambu;?></td>
                                        <td><?php $visi = limit_words($value->keterangan_rambu, 10);
                                            echo $visi."...";?></td>
                                        <td>
                                            
                                            <a href="<?php echo base_url('rambu/detail/'.$value->id_rambu);?>"> <i class="glyphicon glyphicon-eye-open"></i></a>
                                        <?php if ($this->session->userdata('akses') == 1):?>
                                            <a href="<?php echo base_url('rambu/hapus/'.$value->id_rambu);?>" onclick="return confirm('Anda yakin menghapus data pemilik?')"><i class="glyphicon glyphicon-trash"></i></a>
                                        
                                            <a href="<?php echo base_url('rambu/edit/'.$value->id_rambu);?>"><i class="glyphicon glyphicon-pencil"></i></a><?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>