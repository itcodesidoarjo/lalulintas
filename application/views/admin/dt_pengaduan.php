<div class="breadcome-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcome-list single-page-breadcome">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="breadcome-heading">
                                   
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <ul class="breadcome-menu">
                                    <li><a href="#">Data Pengaduan</a> <span class="bread-slash"></span>
                                    </li>
                                    <li><span class="bread-blod"></span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Static Table Start -->
<div class="data-table-area mg-tb-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="sparkline13-list">
                    <div class="sparkline13-hd">
                        <div class="main-sparkline13-hd">
                            <h1>Data Pengaduan <span class="table-project-n"></span> </h1>
                            <?php if ($this->session->userdata('akses') != 1):?>
                            <a href="<?php echo base_url('pengaduan/form_pengaduan');?>" type="submit" class="btn btn-success"><i class="fa fa-plus"></i>  Tambah</a>
                        <?php endif; ?>
                        </div>
                    </div>
                    <div class="sparkline13-graph">
                        <div class="datatable-dashv1-list custom-datatable-overright">
                            <div id="toolbar">
                            </div>
                            <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="false" data-show-pagination-switch="false" data-show-refresh="false" data-key-events="false" data-show-toggle="false" data-resizable="false" data-cookie="false"
                                data-cookie-id-table="saveId" data-show-export="false" data-click-to-select="false" data-toolbar="#toolbar">
                                  <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Foto/Bukti</th>
                                        <th>Isi Pengaduan</th>
                                        <th>Tanggal Pengaduan</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>

                                </thead>
                                 <tbody>
                                    <?php 
                                    $no=1;
                                    foreach($pengaduan as $value) : ?>
                                    <tr>
                                        <td><?=$no++?></td>
                                        <td>
                                            <img width="200px" height="200px" class="img-thumbnail" src="<?php echo base_url() . 'asset/img/'.$value->foto_pengaduan?>"/>
                                        </td>
                                        <td><?=$value->isi_pengaduan;?></td>
                                        <td><?=$value->tanggal_pengaduan;?></td>
                                        <td><?php
                                            if ($value->status_pengaduan==1) :?>
                                               <span class="label label-warning">Belum Dikonfimasi</span>
                                            <?php endif; 
                                             if ($value->status_pengaduan==2) :?>
                                               <span class="label label-success">Sudah di Konfirmasi</span>
                                            <?php endif; 
                                             if ($value->status_pengaduan==3) :?>
                                               <span class="label label-danger">Batal</span>
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <?php if ($this->session->userdata('akses') == 1):?>
                                            <?php if ($value->status_pengaduan==1) :?>
                                            <a href="<?php echo base_url('pengaduan/konfirmasi/'.$value->id_pengaduan);?>" onclick="return confirm('Anda yakin mengkonfirmasi pengaduan?')"> <i class="fa fa-check-circle-o fa-6" title="Konfirmasi"></i></a> 
                                             <?php endif; ?>
                                             <?php endif; ?>
                                            <a href="<?php echo base_url('pengaduan/detail/'.$value->id_pengaduan);?>"> <i class="glyphicon glyphicon-eye-open" title="Detail"></i></a>
                                            <?php if ($value->status_pengaduan==1) :?>
                                            <a href="<?php echo base_url('pengaduan/batal/'.$value->id_pengaduan);?>" onclick="return confirm('Anda yakin membatalkan pengaduan?')" title="Batal"><i class="glyphicon glyphicon-trash"></i></a>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>